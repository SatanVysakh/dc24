from datetime import timedelta
from django.http import JsonResponse
from django.utils import timezone
from wafer.schedule.models import ScheduleItem
from django.shortcuts import render
from .forms import VisaRequestPassportDetailsForm, VisaRequestInviteeDetailsForm, VisaRequestTravelDetailsForm, VisaRequestGuaranteeDetailsForm

def now_or_next(request, venue_id):
    talk_now = False
    talk_next = False
    reload_seconds = 60
    talk = None

    now = timezone.now()
    # now
    try:
        item = ScheduleItem.objects.filter(
            venue=venue_id,
            talk__isnull=False,
            slots__start_time__lte=now,
            slots__end_time__gte=now,
        ).order_by("slots__start_time")[0]
        duration = item.get_duration_minutes()
        end = item.get_start_datetime() + timedelta(minutes=duration)
        reload_seconds = (end - timezone.now()).total_seconds()

        talk_now = True
        talk = item.talk_id
    except IndexError:
        pass

    if not talk:
        # next
        try:
            item = ScheduleItem.objects.filter(
                venue=venue_id,
                talk__isnull=False,
                slots__start_time__gte=now
            ).order_by("slots__start_time")[0]
            reload_seconds = (item.get_start_datetime() - timezone.now()).total_seconds()
            talk_next = True
            talk = item.talk_id
        except IndexError:
            pass

    return JsonResponse({
        "now": talk_now,
        "next": talk_next,
        "reload_seconds": reload_seconds,
        "talk": talk,
    })

def invitation_request_form(request):
    if request.user.is_authenticated:
        email_body_prefix = f"Hello, I'm {request.user.get_full_name()}({request.user.username}). And I'm requesting visa invitation to attend DebConf with following details:"
    else:
        email_body_prefix = f"Hello. I'm requesting visa invitation to attend DebConf with following details:"
    context = {
        "title": "Submit Visa Invitation Request",
        "is_login_required": True,
        "email_title_js_template":"`DC24Visa: ${document.getElementsByName('surname')[0].value}/${document.getElementsByName('givennames')[0].value}`",
        "email_body_prefix": email_body_prefix,
        "email_recipient": "visa@debconf.org",
        "alert_text": "Email client or app will open with content generated from the form you've filled in. \
            Attach the following documents, then send the email to complete your request.\n \
            1. Copy of your passport\n \
            2. Travel insurance policy - In case you requested letter of guarantee and signed up for it already",
        "forms": [VisaRequestPassportDetailsForm, VisaRequestInviteeDetailsForm, VisaRequestTravelDetailsForm, VisaRequestGuaranteeDetailsForm]}
    return render(request, "forms/email-send-form.html", context)