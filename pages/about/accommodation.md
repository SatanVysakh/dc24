---
name: Accommodation
---

# Accommodation

Conference-provided accommodation will be located at the [Student's
dormitory (Sejong-1 gwan)][dormitory], Pukyong National University,
about 100m from the conference venue, Engineering building 1.

[dormitory]: https://www.openstreetmap.org/way/1238066417

<a class="btn btn-info" role="button"
href="https://www.openstreetmap.org/way/1238066417">
  <i class="fa fa-map" aria-hidden="true"></i> Map
</a>

Each room accommodates two people, and the dorms are very strictly
segregated by gender.
Male and female dorms are in separate sections of the building, and
attendees are not permitted to enter the section of the opposite gender.
Unfortunately, this means that we are unable to accommodate mixed-gender
couples in the same dorm room this year.

We understand that this traditional gender separation is problematic for
some attendees.
Contact the registration team if you need alternative accommodation.

Each room contains two single beds, desks, and closet space, as well as
a small bathroom.
The bathrooms are in a "wet room" style with no separate shower cubicle.

There is a curfew in place between 1 and 5 AM.

Korea is a "shoes off" culture in domestic spaces, and this also applies to the dorms.
Attendees are strongly encouraged to bring a pair of flip flops for use
in the bathroom, and slippers for use in the room itself, if they
desire.
The bedrooms and the bathrooms will be shared between 2 people.

The room Wi-Fi and wired LAN ports will be provided by the accommodation
facilities.

<div class="row mb-3">
    <div class="col">
        <img class="img img-fluid" src="{% static "img/dorm-room.jpg" %}">
    </div>
    <div class="col">
        <img class="img img-fluid" src="{% static "img/dorm-bathroom-1.jpg" %}">
    </div>
    <div class="col">
        <img class="img img-fluid" src="{% static "img/dorm-bathroom-2.jpg" %}">
    </div>
</div>

## Self-paid accommodation

DebConf is providing beds for people who want to pay for accommodation
in the same dormitories, at PKNU.

The cost is 21 EUR/night.
If you want a private room just for yourself, the cost is 42 EUR/night.

If number of rooms is filled, we will share that information here.

##  Hotels

If you are looking for a hotel nearby the venue, here is a list:

- [Kent Hotel Gwangalli by Kensington](https://www.tripadvisor.com/g297884-d10423568)
- [Hotel Aqua Palace](https://www.tripadvisor.com/g297884-d1162841)
- [Homers Hotel](http://www.homershotel.com/view/index.do?SS_SVC_LANG_CODE=ENG)
- [Hotel Central Bay](http://www.centralbay.co.kr/view/index.do?SS_SVC_LANG_CODE=ENG)
- [Hotel 1](https://www.tripadvisor.com/g297884-d14008119)

You may book a room in nearby hotels or hostels in Busan.
If you don't see any availability on their website, we'd suggest
checking booking aggregator websites or contacting the hotel directly.
