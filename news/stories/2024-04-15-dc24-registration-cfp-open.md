---
title: DebConf24 registration and CfP are open
---

Registration and the Call for Proposals for DebConf24 are now open. The
25th edition of the Debian annual conference will be held from *July
28th to August 4th, 2024, in Pukyong National University, Busan,
Republic of Korea.*

The main conference will be preceded by DebCamp, which will take place
from July 21st to July 27th, 2024.

The registration form can be accessed on the DebConf 24 website.
After creating an account, click ["register"][1] in the profile section.

[1]: https://debconf24.debconf.org/register/

As always, basic registration for DebConf is free of charge for
attendees. If you are attending the conference in a professional
capacity or as a representative of your company, we kindly ask that you
consider registering in one of our paid categories to help cover the
costs of organizing the conference and to support subsidizing other
community members.

The last day to register with guaranteed swag is 30 May.

We also encourage eligible individuals to apply for a diversity
bursary. Travel, food, and accommodation bursaries are also available. More
details can be found on the [bursary info page][2].

[2]: https://debconf24.debconf.org/about/bursaries/

The last day to apply for a bursary is 8th May. Applicants should
receive feedback on their bursary application by 23rd May.

The call for proposals for talks, discussions and other activities is
also open. To submit a proposal you need to create an account on the
website, and then use the ["Submit Talk"][3] button in the profile
section.
The last day to submit and have your proposal be considered for the
main conference schedule, with video coverage guaranteed, is June
2nd.

[3]: https://debconf24.debconf.org/talks/new/

DebConf24 is also accepting sponsors. Interested companies and
organizations may contact the DebConf team through sponsors@debconf.org
or [visit the DebConf24 website][4].

[4]: https://debconf24.debconf.org/sponsors/become-a-sponsor/

See you in Busan,

The DebConf 24 Team
